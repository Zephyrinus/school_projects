import time
import RPi.GPIO as GPIO
import Keypad
from mfrc522 import SimpleMFRC522
import mysql.connector
import hashlib
from pyfingerprint.pyfingerprint import PyFingerprint
from PCF8574 import PCF8574_GPIO
from Adafruit_LCD1602 import Adafruit_CharLCD
GPIO.setwarnings(False)

db = mysql.connector.connect(
  host="localhost",
  user="admin",
  passwd="secterm",
  database="login_system"
)
cursor = db.cursor()
reader = SimpleMFRC522()
#lcd = LCD.Adafruit_CharLCD(4, 24, 23, 17, 18, 22, 16, 2, 4);

ROWS = 4        # number of rows of the Keypad
COLS = 4        #number of columns of the Keypad
keys =  [   '1','2','3','A',    #key code
            '4','5','6','B',
            '7','8','9','C',
            '*','0','#','D'     ]
rowsPins = [31,32,33,35]        #connect to the row pinouts of the keypad
colsPins = [36,37,38,40]        #connect to the column pinouts of the keypad

def access_granted():
    message_a = "Access granted"
    lcd_message(message_a)
    print(message_a)
def access_denied():
    message_b = "Access denied"
    lcd_message(message_b)
    print(message_b)
def lcd_message(message):
        mcp.output(3,1)     # turn on LCD backlight
        lcd.begin(16,2)     # set number of LCD lines and columns
        lcd.clear()
        lcd.setCursor(0,0)  # set cursor position
        lcd.message(message)   # display the time
        #time.sleep(2)
        #lcd.clear()
def keypad():

    keypad = Keypad.Keypad(keys,rowsPins,colsPins,ROWS,COLS)    #creat Keypad object
    keypad.setDebounceTime(50)      #set the debounce time
    while(True):
        key = keypad.getKey()       #obtain the state of keys
        if(key != keypad.NULL):     #if there is key pressed, print its key code.
            print ("You Pressed Key : %c "%(key))
            break
    return key
def finger_search():
    ## Tries to initialize the sensor
    try:
        f = PyFingerprint('/dev/serial0', 57600, 0xFFFFFFFF, 0x00000000)

        if ( f.verifyPassword() == False ):
            raise ValueError('The given fingerprint sensor password is wrong!')

    except Exception as e:
        print('The fingerprint sensor could not be initialized!')
        print('Exception message: ' + str(e))
        exit(1)

    ## Gets some sensor information
    print('Currently used templates: ' + str(f.getTemplateCount()) +'/'+ str(f.getStorageCapacity()))

    ## Tries to search the finger and calculate hash
    try:
        print('\n')
        print('Waiting for finger...')
        lcd_message("waiting for \nfinger......")

        ## Wait that finger is read
        while ( f.readImage() == False ):
            pass

        ## Converts read image to characteristics and stores it in charbuffer 1
        f.convertImage(0x01)

        ## Searchs template
        result = f.searchTemplate()

        positionNumber = result[0]
        accuracyScore = result[1]

        if ( positionNumber == -1 ):
            print('No match found!')
            print('Access denied')
            lcd_message("No Match!")
            return(0)
        else:
            print('Found template at position #' + str(positionNumber))
            print('The accuracy score is: ' + str(accuracyScore))

        ## OPTIONAL stuff
        ##

        ## Loads the found template to charbuffer 1
        f.loadTemplate(positionNumber, 0x01)

        ## Downloads the characteristics of template loaded in charbuffer 1
        characterics = str(f.downloadCharacteristics(0x01)).encode('utf-8')
        hashed_fingerprint = hashlib.sha256(characterics).hexdigest()
        ## Hashes characteristics of template
        output = (positionNumber)
        print(output)
        print('\n')
        return output
        
    
    except Exception as e:
        print('Operation failed!')
        print('Exception message: ' + str(e))
        exit(1)
def finger_enroll():
    ## Tries to initialize the sensor
    try:
        f = PyFingerprint('/dev/serial0', 57600, 0xFFFFFFFF, 0x00000000)

        if ( f.verifyPassword() == False ):
            raise ValueError('The given fingerprint sensor password is wrong!')

    except Exception as e:
        print('The fingerprint sensor could not be initialized!')
        print('Exception message: ' + str(e))
        exit(1)

    ## Gets some sensor information
    print('Currently used templates: ' + str(f.getTemplateCount()) +'/'+ str(f.getStorageCapacity()))

    ## Tries to enroll new finger
    try:
        print('Waiting for finger...')

        ## Wait that finger is read
        while ( f.readImage() == False ):
            pass

        ## Converts read image to characteristics and stores it in charbuffer 1
        f.convertImage(0x01)

        ## Checks if finger is already enrolled
        result = f.searchTemplate()
        positionNumber = result[0]

        if ( positionNumber >= 0 ):
            print('Template already exists at position #' + str(positionNumber))
            return(0)

        print('Remove finger...')
        time.sleep(2)

        print('Waiting for same finger again...')

        ## Wait that finger is read again
        while ( f.readImage() == False ):
            pass

        ## Converts read image to characteristics and stores it in charbuffer 2
        f.convertImage(0x02)

        ## Compares the charbuffers
        if ( f.compareCharacteristics() == 0 ):
            raise Exception('Fingers do not match')

        ## Creates a template
        f.createTemplate()

        ## Saves template at new position number
        positionNumber = f.storeTemplate()
        print('Finger enrolled successfully!')
        print('New template position #' + str(positionNumber))

    except Exception as e:
        print('Operation failed!')
        print('Exception message: ' + str(e))
        exit(1)
def rfid_search():
    id, text = reader.read()
    cursor.execute("Select id, name FROM users WHERE rfid_uid="+str(id))
    result = cursor.fetchone()
    if cursor.rowcount >= 1:
        print('Welcome ' + result[1])
        lcd_message('Welcome ' + result[1])
        time.sleep(1)
        cursor.execute("Select fingerprint_id, name FROM users WHERE rfid_uid="+str(id))
        user_hash, name = cursor.fetchone()
        output = str(finger_search())
        #print(output) # debugging
        #print(user_hash) # debugging 
        if user_hash == output:
            access_granted()

            cursor.execute("INSERT INTO attendance (user_id) VALUES (%s)", (result[0],) )
            db.commit()
            time.sleep(1)
        else:
            print("Access denied")
    else:
        print('User does not exist..')
        time.sleep(1)

try:
    mcp = PCF8574_GPIO(0x27)
except:
    print ('I2C Address Error !')
    exit(1)
# Create LCD, passing in MCP GPIO adapter.
lcd = Adafruit_CharLCD(pin_rs=0, pin_e=2, pins_db=[4,5,6,7], GPIO=mcp)

while True:
    lcd_message(" System ready.... ")
    print("system ready....")
    keypress = keypad()
    if keypress == keypress:
        try:
            lcd_message(" Press * ")
            keypress = keypad()
            if keypress == "A":
                print('Register card')
                lcd_message("Register card")
                id, text = reader.read()
                print('select user in db')
                hash = finger_enroll()
                output = finger_search()
            elif keypress == "*":
                print('Search')
                lcd_message("Place card on \nRFID scanner")
                rfid_search()
                time.sleep(3)
                lcd_message("Engaging lock... \nin")
                time.sleep(2)
                lcd_message("3.")
                time.sleep(0.5)
                lcd_message("2..")
                time.sleep(0.5)
                lcd_message("1...")
                time.sleep(0.5)
                lcd_message("System locked...")
                time.sleep(2)
                continue

            if cursor.rowcount >= 1:
                overwrite = input('Overwrite (Y/N)?')
                if overwrite[0] == 'Y' or overwrite[0] == 'y':
                    time.sleep(1)
                    sql_insert = "UPDATE users SET name = %s, fingerprint_id = hashfinger WHERE rfid_uid=%s"
                    
            else:
                sql_insert = "INSERT INTO users (name, rfid_uid, fingerprint_id, pin) VALUES (%s, %s, %s, 123)" 
                print('Enter new name')
                new_name = input('Name: ')
                cursor.execute(sql_insert, (new_name, id, output))
                db.commit()
                print('User ' + new_name + 'Saved')
                time.sleep(2)
        except KeyboardInterrupt:
            print('stopping program')
        finally:
            GPIO.cleanup()
