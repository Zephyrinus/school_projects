# Projekt_Datacenter_GR14


Scrum Link: 


## Part of this team are 

 • Emil Simonsen - @emil.privat 

 • Romulus V. Prundis - @Zephyrinus 

 • Bogdan Buterchi - @bogdan7978

 • Simon Storm - @V1ncit



## Case

Security improvement for the Fredericia Datacenter


## Requirements:


## Parts needed

•1 ea raspberrypi zero 2

•1 ea RFID reader (card reader)

•10 ea RFID cards or tokens

•1 ea fingerprint scanner

•1 ea touchscreen

## Project problem statement

 - Is it possible to limit access to the datacenter with accesscard, pincode, or fingerprint auth, or a combination of two or more.
 - Is it possible to have different access levels?
 - Can the studentcard already in use be used for access?


### Reference links:
RFID reader - https://pimylifeup.com/raspberry-pi-rfid-rc522/
Display - https://www.electroniclinic.com/raspberry-pi-16x2-lcd-i2c-interfacing-and-python-programming/
Fingerprint sensor - https://www.youtube.com/watch?v=fQKFf8Ei8nI



