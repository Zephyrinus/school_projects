---
title: 'Group14_Biohazard emergency rover project'
subtitle: 'Project description'
authors: ['Romulus Prundis \<rvpr37603@edu.ucl.dk\>', 'Emil Simonsen \<esi.@edu.ucl.dk\>'], Simon Storm, Bogdan Buterchi, >bdbu37436@edu.ucl.dk
date: \today
left-header: \today
right-header: Project plan
skip-toc: false
skip-tof: true
---

Scrum Link:
https://trello.com/b/2AGh4Ybm/scrum-board


# Group_14 - 31.01.2022
This is Group 14 project - Biohazard emergency rover project, for the second semester at UCL, the topic is building a rover from scatch that will be able to lift dangerous substances.

At the end the robot will compete with other robots from KEA.


# Part of this team are 

 • Emil Simonsen - @emil.privat 

 • Romulus V. Prundis - @Zephyrinus 

 • Bogdan Buterchi - @bogdan7978

 • Simon Storm - @V1ncit



# Case

Team 14 rover will have DC motors for the wheels, a robot arm controlled by servo motors for lifting dangerouns substances.
All the code was developed in python, with a small injection of html for the controller on the webserver.


# Requirements:
• The rover must be able to navigate on a planar surface

• The rover must be able to be controlled wirelessly, using a webserver hosted internally on the rover.

• The rover must be battery powered.

• The rover must have a battery life of minimum 15 minutes

• The rover must be able to pick up a vial, with a diameter of 20mm

• The rover must be able to automatically navigate through curvy roads.

• The rover must be able to turn, within a turning radius of 1 meters

• The rover must be able to make audible sounds, when reversing.




For more info: https://docs.google.com/document/d/1MIpzsUodZX9vNK-znirCAODf1JDXLdnvN1jvDsLmRp8/edit#

Result: https://gitlab.com/emil.privat/group_14/-/blob/main/Group14.jpg




