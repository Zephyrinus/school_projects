import machine
import time
pin1 = machine.Pin(32) # replace with your chosen pin
servo1 = machine.PWM(pin1, freq=50)
#servo1.duty(20) # example of how to operate servo
while True:
    for i in range (20,120,1):
        servo1.duty(i)
        time.sleep(0.05)