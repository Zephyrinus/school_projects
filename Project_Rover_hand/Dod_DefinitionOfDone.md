# Defition Of Done (DOD)

In this file we all need to agree on what is the definition of done of am item for all of us.
An item or user story is part of the project development. 
Can be from soldering the pins for a sensor to writing a program that can read temperature, 
another one for example is setting the MQTT protocol (publish - subscribe model).
During every sprint we will have to solve different items / user stories.

How do we define DOD (Definition of done)?

When is an item / user story done? And when can be moved from in progress to done?
