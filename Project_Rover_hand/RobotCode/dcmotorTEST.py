from dcmotor import DCMotor
from machine import Pin, PWM
from time import sleep

def motor_test(motor):
    motor.forward(60)


frequency = 15000
#right motor
STBY_D1 = Pin(4, Pin.OUT) # Replace x

PHA_D1A = Pin(17, Pin.OUT) # Replace y
enable_D1A = PWM(Pin(16), frequency)

PHA_D1B = Pin(0, Pin.OUT) # Replace y
enable_D1B = PWM(Pin(15), frequency)

dc_motor_right_1 = DCMotor(STBY_D1, PHA_D1A, enable_D1A)
dc_motor_right_2 = DCMotor(STBY_D1, PHA_D1B, enable_D1B)

#left motor
STBY_D2 = Pin(14, Pin.OUT) # Replace x

PHA_D2A = Pin(13, Pin.OUT) # Replace y
enable_D2A = PWM(Pin(12), frequency)

PHA_D2B = Pin(27, Pin.OUT) # Replace y
enable_D2B = PWM(Pin(26), frequency)

dc_motor_left_1 = DCMotor(STBY_D2, PHA_D2A, enable_D2A)
dc_motor_left_2 = DCMotor(STBY_D2, PHA_D2B, enable_D2B)



motor_test(dc_motor_left_1)
motor_test(dc_motor_right_1)
motor_test(dc_motor_left_2)
motor_test(dc_motor_right_2)


