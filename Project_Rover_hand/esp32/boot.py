
try:
  import usocket as socket
except:
  import socket

from time import sleep
from machine import Pin, PWM
import network

import esp
esp.osdebug(None)

import gc
gc.collect()

buzzer = Pin(32, Pin.OUT, Pin.PULL_DOWN)

# wifi hotspot credentials for Rover to connect to
ssid = 'Team14stf'
password = 'fantastic'

station = network.WLAN(network.STA_IF)

station.active(True)
station.connect(ssid, password)

print("starting loop")
#while station.isconnected() == False:
#  pass

#print('Connection successful');
#print(station.ifconfig())

sleep(1)
buzzer.value(1)
sleep(0.5)
buzzer.value(0)
print('starting main')