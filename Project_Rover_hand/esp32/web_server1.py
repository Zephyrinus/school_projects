import time
try:
    import usocket as socket
except:
    import socket

from machine import Pin, PWM
import network
from dcmotor import DCMotor

import auto_pilot
import _thread
import globalvar
import servo_control

print("Running Webserver")

# Setup AP
station = network.WLAN(network.AP_IF)
station.active(True)
station.config(essid = 'Team14stf')
station.config(authmode=3, password = 'fantastic')

while station.isconnected() == False:
    pass

print('Connection successful')
print(station.ifconfig())

def motorcontrol(left, right):
    motorL.value(left)
    motorR.value(right)

def web_page(request):
    motor_state = "Stopped"
    if request.find('/?forward') > 0:
        motor_state = "Going Forward"
        DCMotor.move_forward()

    if request.find('/?left') > 0:
        motor_state = "Going Left"
        DCMotor.turn_left()
   
    if request.find('/?right') > 0:
        motor_state = "Going Right"
        DCMotor.turn_right()

    if request.find('/?stop') > 0:
        motor_state = "Stopped"
        globalvar.autopilot_state = False
        DCMotor.motor_stop()
        
    if request.find('/?autopilot') > 0:
        globalvar.autopilot_state = True
        _thread.start_new_thread(auto_pilot.autopilot,())
        
    if request.find('/?deactivate-arm') > 0:
        motor_state = "Arm deactivated"
        servo_control.deinit_servos()

    if request.find('/?activate-arm') > 0:
        motor_state = "Arm activated"
        servo_control.init_servos()
        
    if request.find('/?arm-left') > 0:
        servo1_int = servo1_int
        servo1_duty = servo1_int + 5
        servo_control.servo1_movement(servo1_duty)
        
        

    html = '''<html><head> <title>Rover Web Server</title>
    <head>
    <title>ESP IOT DASHBOARD</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="favicon.png">
    </head>
    <body> <h1>Biohazard Rover Web Server</h1>
    
    <p>Rover : <strong>''' + motor_state + '''</strong></p>
    
    <p><a href='/?forward'><button class="button">Forward</button></a></p>
    
    <p><a href='/?left'><button class="button">LEFT</button></a>
    <a href='/?right'><button class="button" >RIGHT</button></a></p>
    
    <p><a href='/?stop'><button class="button">STOP</button></a></p>

    <p><a href='/?autopilot'><button class="button">Autopilot</button></a></p>
    
    <p><a href='/?activate-arm'><button class="button">Activate</button></a><a href='/?deactivate-arm'><button class="button">Deactivate</button></a></p>
    <p><a href='/?arm_left'><button class="button">Arm left</button></a> <a href='/?arm_right'><button class="button">Arm right</button></a></p>
    <p><a href='/?arm_forward'><button class="button">Arm forward</button></a> <a href='/?arm_backward'><button class="button">Arm backward</button></a></p>
    <p><a href='/?arm_down'><button class="button">Arm down</button></a> <a href='/?arm_up'><button class="button">Arm up</button></a></p>
    <p><a href='/?gripper_open'><button class="button">Gripper open</button></a> <a href='/?gripper_close'><button class="button">Gripper close</button></a></p>    
    

    </body></html>    
    '''

    return html
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

while True:
    conn, addr = s.accept()
    print('Got a connection from %s' % str(addr))
    request = conn.recv(1024)
    request = str(request)
    print('Content = %s' % request)
    response = web_page(request)
    conn.send(response)
    conn.close()