from dcmotor import DCMotor
from machine import Pin, PWM
from hcsr04 import HCSR04
from time import sleep
ap_state = 0


sensor_left = HCSR04(trigger_pin = 33, echo_pin = 25, echo_timeout_us = 10000)
sensor_right = HCSR04(trigger_pin = 18, echo_pin = 5, echo_timeout_us = 10000)

frequency = 15000
buzzer = Pin(32, Pin.OUT, Pin.PULL_DOWN)

#right motor
STBY_D1 = Pin(4, Pin.OUT) # Replace x

PHA_D1A = Pin(17, Pin.OUT) # Replace y
enable_D1A = PWM(Pin(16 ), frequency)

PHA_D1B = Pin(0, Pin.OUT) # Replace y
enable_D1B = PWM(Pin(15), frequency)

dc_motor_right_1 = DCMotor(STBY_D1, PHA_D1A, enable_D1A)
dc_motor_right_2 = DCMotor(STBY_D1, PHA_D1B, enable_D1B)

#left motor
STBY_D2 = Pin(14, Pin.OUT) # Replace x

PHA_D2A = Pin(13, Pin.OUT) # Replace y
enable_D2A = PWM(Pin(12), frequency)

PHA_D2B = Pin(27, Pin.OUT) # Replace y
enable_D2B = PWM(Pin(26), frequency)

dc_motor_left_1 = DCMotor(STBY_D2, PHA_D2A, enable_D2A)
dc_motor_left_2 = DCMotor(STBY_D2, PHA_D2B, enable_D2B)

def feedback(distance_left, distance_right):

    if left_dist < 10:
        print('turn right')
        turn_right()
    elif right_dist < 10:
        print('turn left')
        turn_left()
    else:
        move_forward()
        print('moving forward')


        
def turn_left():
    dc_motor_right_1.forward(60)
    dc_motor_right_2.forward(60)
    dc_motor_left_1.backwards(0)
    dc_motor_left_2.backwards(0)

def turn_right():
    dc_motor_right_1.backwards(0)
    dc_motor_right_2.backwards(0)
    dc_motor_left_1.forward(60)
    dc_motor_left_2.forward(60)
    
def move_forward():
    dc_motor_right_1.forward(60)
    dc_motor_right_2.forward(60)
    dc_motor_left_1.forward(60)
    dc_motor_left_2.forward(60)

def motor_stop():
    dc_motor_right_1.stop()
    dc_motor_right_2.stop()
    dc_motor_left_1.stop()
    dc_motor_left_2.stop()

right_dist = [0,0,0,0,0,0,0,0,0,0]
left_dist = [0,0,0,0,0,0,0,0,0,0]
true_right = 0
true_left = 0


while ap_state == 1:
    distance_left = sensor_left.distance_cm()
    distance_right = sensor_right.distance_cm()
    if distance_left < 1:
        print('error left')
        continue
    if distance_right < 1:
        print('error right')
        continue
    else:
        right_dist.append(distance_right)
        del right_dist[0]
        true_right = sum(right_dist) / len(right_dist) 
        print(true_right)
        left_dist.append(distance_left)
        del left_dist[0]
        true_left = sum(left_dist) / len(left_dist)
        print(true_left)
        
        if true_left < 20:
            turn_right()
        elif true_right < 20:
            turn_left()
        else:
            move_forward()

            
            

        
        
'''
    else:
        feedback(distance_left, distance_right)
        print('Distance left sensor:', distance_left, 'cm' ' - ' 'Distance right sensor:', distance_right, 'cm')
        sleep(0.5)
'''
